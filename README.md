# MLBIOINF_P-2023

## Team Name
The Avengers

## Team members
1. Saniya Nankeni (saniyaumen99 )
2. Anuraj Suman ( anuras92 ) 
3. Sai Preetham Kumar (saipreen98) 

## How to run the code ?

1. Download the ipynb file from the respective folders ( PCR, PCLR, RF, SVMLin or SVMRBF )
2. Open this downloaded notebook in google colab
3. Download the data from the Dataset folder
4. Upload the downloaded dataset to Google colab
5. Run the code blocks in ipynb file and observe the results 


